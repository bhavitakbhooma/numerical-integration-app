#ifndef ERR_LIB_H
#define ERR_LIB_H

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

void client_usage_err();
void connection_err(char* portnum);
void file_err(char* filename);
void integration_fail();
void communication_err();
void syntax_err(int linenum);
void space_err(int linenum);
void bounds_err(int linenum);
void segments_err(int linenum);
void threads_err(int linenum);
void segment_not_multiple_err(int linenum);
void function_expr_err(char* functon, int linenum);
void server_usg_err();
void socket_err();

#endif