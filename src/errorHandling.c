#include "errorHandling.h"

#define CLIENT_USG_ERR 1
#define CONNECT_ERR 2
#define COMMUNICATION_ERR 3
#define FILE_ERR 4
#define SERVER_USG_ERR 1
#define SOCKET_ERR 3

// intclient ERROR HANDLING ----------------------------------------------

// Terminates the program with error code '1' 
// if intclient command line is invalid.
void client_usage_err() {
    fprintf(stderr, "Usage: intclient [-v] portnum [jobfile]\n");
    exit(CLIENT_USG_ERR);
}

// Terminates the program with error code '2' 
// if intclient is unable to connect to the port.
void connection_err(char* portnum) {
    fprintf(stderr, "intclient: unable to connect to port %s\n", portnum);
    exit(CONNECT_ERR);
}

// Terminates the program with error code '4' 
// if inclient is unable to open the file.
void file_err(char* filename) {
    fprintf(stderr, "intclient: unable to open \"%s\" for reading\n", 
            filename);
    exit(FILE_ERR);
}

// Prints error msg to stderr if response from server is integration failure.
void integration_fail() {
    fprintf(stderr, "intclient: integration failed\n");
}

// Terminates the program with error code '3' if intclient receives 
// an unexpected end of file or other error on the socket connection, 
// or receives an badly formed or unexpected response from intserver.
void communication_err() {
    fprintf(stderr, "intclient: communications error\n");
    exit(COMMUNICATION_ERR);
}

// Prints error message to stderr if intclient identifies 
// a syntax error when parsing the job file.
// Takes line number as @param.
void syntax_err(int linenum) {
    fprintf(stderr, "intclient: syntax error on line %d\n", linenum);
}

// Prints error message to stderr if intclient detects whitespace characters.
// Takes line number as @param.
void space_err(int linenum) {
    fprintf(stderr, "intclient: spaces not permitted in expression (line %d)\n"
            , linenum);
}

// Prints error message to stderr if upper bound is less than lower bound.
// Takes line number as @param.
void bounds_err(int linenum) {
    fprintf(stderr, 
            "intclient: upper bound must be " 
            "greater than lower bound (line %d)\n", linenum);
}

// Prints error message to stderr if value of segments field is not positive.
// Takes line number as @param.
void segments_err(int linenum) {
    fprintf(stderr, 
            "intclient: segments must be a "
            "positive integer (line %d)\n", linenum);
}

// Prints error message to stderr if value of threads field is not positive.
// Takes line number as @param.
void threads_err(int linenum) {
    fprintf(stderr, 
            "intclient: threads must be a "
            "positive integer (line %d)\n", linenum);
}

// Prints error message to stderr if segments field is not multiple of threads.
// Takes line number as @param.
void segment_not_multiple_err(int linenum) {
    fprintf(stderr, 
            "intclient: segments must be an integer "
            "multiple of threads (line %d)\n", linenum);
}

// Prints error message to stderr if function field is not a valid expression.
// Takes funtion and line number as @param.
void function_expr_err(char* function, int linenum) {
    fprintf(stderr, 
            "intclient: bad expression \"%s\" (line %d)\n", function, linenum);
}

// intserver ERROR HANDLING ----------------------------------------------

// Terminates the program with exit status '1' 
// if intserver command line is invalid.
void server_usg_err() {
    fprintf(stderr, "Usage: intserver portnum [maxthreads]\n");
    exit(SERVER_USG_ERR);
}

// Terminates the program with exit status '3'
// if intserver is unable to open either the ephemeral or specific port.
void socket_err() {
    fprintf(stderr, "intserver: unable to open socket for listening\n");
    exit(SOCKET_ERR);
}




