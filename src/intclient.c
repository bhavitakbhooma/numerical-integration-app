#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <fcntl.h>
#include <ctype.h>
#include <netdb.h>
#include <csse2310a3.h>
#include <csse2310a4.h>
#include <tinyexpr.h>
#include <limits.h>
#include "errorHandling.h"

#define MIN_CALC_PARAM 5
#define BUFF_SIZE 10000
#define TEMP_BUFF_SIZE 500
#define STATUS_OK 200
#define BAD_STATUS 400

// Structure to store each client's jobfile fields.
typedef struct ClientFields {
    char* function;
    double lower;
    double upper;
    int segments;
    int threads;
} ClientFields;

// Structure to store each client details.
typedef struct ServerConnection {
    int server;
    FILE* readResponse;
    FILE* writeRequest;
} ServerConnection;

// Takes two strings as @params.
// Checks if two strings are equal and returns true.
bool is_equal(char* str1, char* str2) {
    return !strcmp(str1, str2);
}

// Takes argc and argv as @params.
// Checks if argv[1] contains "-v" if yes then changes verbose flag to true.
// Also checks if "-v" is present in any other position other than argv[1], 
// if so then prints client usage error.
// Returns the verbose flag status.
bool check_verbose(int argc, char** argv) {
    int portIndex = 1;
    bool verbose = false;
    for (int i = 1; i < argc; i++) {
        if (is_equal(argv[i], "-v")) {
            if (i != 1 || verbose) {
                client_usage_err();
            }
            verbose = true;
            portIndex++;
        }
    }
    if (argc == portIndex) {
        client_usage_err();
    }
    return verbose;
}

// Takes line as @param.
// Checks if the line is a comment by comparing first char with "#".
// Returns true if its a comment else returns false.
bool is_comment(char* line) {
    if (line[0] == '#') { 
        return true;
    } else {
        return false;
    }
}

// Takes function expression from job file as @param.
// Checks if there is any space in the function expression.
// If there is space then returns true else returns false.
bool is_space(char* function) {
    for (int i = 0; function[i] != '\0'; i++) {
        if (isspace(function[i])) {
            return true;
        }
    }
    return false;
}

// Takes line as @param.
// Checks if the line is empty.
// If its empty then returns true else returns false.
bool is_empty(char* line) {
    if (!strlen(line)) {
        return true;
    }
    for (int i = 0; line[i]; i++) {
        if (!isspace(line[i])) {
            return false;
        }
    }
    return true;
}

// Takes line as @param.
// Checks if the line is empty or null.
// If its empty or null then returns true else returns false.
bool null_or_empty(char* line) {
    if (!line || !strlen(line)) {
        return true;
    }
    return false;
}

// COMMUNTICATION PROTOCOL ------------------------------------------------

// Used reference code provided in the lectures.
// Takes the port value as @param and attempts to connect to the port on
// localhost and returns file descriptor on sucessful connection. Else,
// generates a connection error and terminates the program.
int connect_to_server(char* port) {
    struct addrinfo* addrInfo = 0;
    struct addrinfo hints;
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_INET; // IPv4
    hints.ai_socktype = SOCK_STREAM;
    int err;
    if ((err = getaddrinfo("localhost", port, &hints, &addrInfo))) {
        freeaddrinfo(addrInfo);
        connection_err(port);
    }
    int fd = socket(AF_INET, SOCK_STREAM, 0); // 0 == use default protocol
    if (connect(fd, (struct sockaddr*)addrInfo->ai_addr, 
            sizeof(struct sockaddr))) {
        freeaddrinfo(addrInfo);
        connection_err(port);
    }
    return fd;
}

// Takes readFile file pointer, content present status, content bytes, 
// buffer and temp buffer as @params.
// Reads as many lines as possible as HTTP content as long as
// thereIsContent and contBytes > 1
void process_content_bytes(FILE* readFile, bool thereIsContent, 
        int contBytes, char* buffer, char* tempBuffer) {
    if (thereIsContent) {
        while (contBytes > 1) {
            if (!fgets(tempBuffer, contBytes + 1, readFile)) {
                // client disconnects
                communication_err();
            }
            strcat(buffer, tempBuffer);
            contBytes -= strlen(tempBuffer);
        }
    }
}

// Takes readFile file pointer as @param.
// Reads as many lines as possible as HTTP buffer, 
// stops reading when encounters an empty new line.
// Checks if the content in the temp buffer is equal to the content- length.
// Returns the HTTP buffer that was read. 
char* read_http_buffer(FILE* readFile) {
    char* buffer = malloc(BUFF_SIZE);
    memset(buffer, 0, BUFF_SIZE);
    
    int limit = BUFF_SIZE;
    int used = 0;
    static char niffer[BUFF_SIZE];
    char* tempBuffer = &niffer[0];
    bool thereIsContent = false;
    int contBytes = 0;

    while (true) {
        memset(niffer, 0, BUFF_SIZE);
        if (!fgets(tempBuffer, BUFF_SIZE - 1, readFile)) {
            // client disconnect
            communication_err();
        }
        used += strlen(tempBuffer);
        if (used >= limit) {
            limit += BUFF_SIZE;
            buffer = realloc(buffer, limit);
        }
        strcat(buffer, tempBuffer);
        if (!strncmp("Content-Length: ", tempBuffer, 
                strlen("Content-Length: "))) {
            if (strncmp("Content-Length: 0", tempBuffer, 
                    strlen("Content-Length: 0"))) {
                sscanf(&tempBuffer[strlen("Content-Length: ")], 
                        "%d", &contBytes);
                thereIsContent = true;
            }
        }
        if (!strcmp(tempBuffer, "\r\n") || !strcmp(tempBuffer, "\n\n") 
                || !strcmp(tempBuffer, "\n")) {
            break;
        }
    }
    process_content_bytes(readFile, thereIsContent, 
                            contBytes, buffer, tempBuffer);
    return buffer;
}

// Takes function expression as @param.
// Creates a buffer to get validate request from server.
// Returns the validate request buffer. 
char* create_validate_buffer(char* function) {
    const char* validateRequest = "GET /validate/%s HTTP/1.1\r\n\r\n";
    char* buffer = malloc(strlen(function) + strlen(validateRequest) + 10);
    sprintf(buffer, validateRequest, function);
    return buffer;
}

// Takes client's jobfile fields and vFlag status as @param.
// Creates a buffer to get integrate request from server.
// Returns the integrate request buffer. 
char* create_integrate_buffer(ClientFields* fields, bool vFlag) {
    const char* integrateRequest = 
            "GET /integrate/%lf/%lf/%u/%u/%s HTTP/1.1\r\n%s\r\n";
    const char* verbose = (vFlag) ? "X-Verbose: yes\r\n" : "";
    char* buffer = malloc(strlen(fields->function) + 
            strlen(integrateRequest) + 100);
    sprintf(buffer, integrateRequest, fields->lower, fields->upper, 
            fields->segments, fields->threads, fields->function, verbose);
    return buffer;
}

// Takes HTTP headers, text, body and response as @params.
// Freeing the HTTP requests.
void free_http(HttpHeader** headers, char* text, char* body, char* response) {
    free_array_of_headers(headers);
    if (text != NULL) {
        free(text);
    }
    if (body != NULL) {
        free(body);
    }
    if (response != NULL) {
        free(response);
    }
}

// Takes function expression, server connection and line number as @params.
// Client sends server the expression to check if its valid.
// Creates a buffer to get validate request from server.
// Creates a response to send to the server.
// If response if null or empty then prints communication error.
// If the parsed response contains less than or equal to 0 buffer of bytes.
// Returns true if the server accepts the requests and validates the function.
bool validate_function_to_server(char* function, 
        ServerConnection* sc, int lineNum) {
    char* buffer = create_validate_buffer(function);
    fprintf(sc->writeRequest, "%s", buffer);
    fflush(sc->writeRequest);
    free(buffer);

    char* response = read_http_buffer(sc->readResponse);

    if (null_or_empty(response)) {
        communication_err();
    }
    
    int parsed;
    int status = 0;
    char* text = NULL;
    char* body = NULL;
    HttpHeader** headers = NULL;
    parsed = parse_HTTP_response(response, strlen(response), 
            &status, &text, &headers, &body);
    if (parsed <= 0) {
        communication_err();
    }
    free(response);
    free_array_of_headers(headers);
    if (status != STATUS_OK) {
        function_expr_err(function, lineNum);
        return false;
    }
    return true;
}

// Takes function expression, server connection, 
// vFlag status and result as @params.
// Creates and sends Integraion request to server, reads HTTP back from server.
// If the response is null or empty prints communication error.
// If the parsed response contains less than or equal to 0 buffer of bytes.
// If vFlag is true then prints partial results. 
// Returns true if integration is successful or false on failure.
bool integrate_to_server(ClientFields* fields, 
        ServerConnection* sc, bool vFlag, double* result) {
    char* request = create_integrate_buffer(fields, vFlag);
    fprintf(sc->writeRequest, "%s", request);
    fflush(sc->writeRequest);
    free(request);

    char* response = read_http_buffer(sc->readResponse);

    if (null_or_empty(response)) {
        communication_err();
    }
    int parsed;
    int status = 0;
    char* text = NULL;
    char* body = NULL;
    HttpHeader** headers = NULL;

    parsed = parse_HTTP_response(response, strlen(response), 
            &status, &text, &headers, &body);
    if (parsed <= 0) {
        communication_err();
    }
    if (status == BAD_STATUS) {
        return false;
    }
    if (vFlag) {
        char** partialResults = split_by_char(body, '\n', 0);
        int i;
        for (i = 0; i < fields->threads; i++) {
            if (null_or_empty(partialResults[i])) {
                communication_err();
            }
            printf("%s\n", partialResults[i]);
        }
        
        if (null_or_empty(partialResults[i]) 
                || sscanf(partialResults[i], "%lf", result) != 1) {
            communication_err();
        }
        free(partialResults);
    } else {
        if (null_or_empty(body) || sscanf(body, "%lf", result) != 1) {
            communication_err();
        }
    }
    return true;
}

// JOBFILE FIELDS PARSING AND VALIDATION ---------------------------------

// Takes client's jobfile fields and line number as @param.
// Verifies the jobfile fields.
// Checks if the function field has space, if not prints err msg.
// Checks if upper is greater than lower bound, if not prints err msg.
// Checks if segment is positive integer, if not prints err msg.
// Checks if thread is positive integer, if not prints err msg.
// Checks if segments is an integer multiple of threads, if not prints err msg.
// If the jobfile fields are valid then returns true else returns false msg.
bool verify_fields(ClientFields* fields, int lineNum) {
    if (is_space(fields->function)) {
        space_err(lineNum);
        return false;
    }
    if (fields->upper <= fields->lower) {
        bounds_err(lineNum);
        return false;
    }
    if (fields->segments <= 0) {
        segments_err(lineNum);
        return false;
    }
    if (fields->threads <= 0) {
        threads_err(lineNum);
        return false;
    }
    if (fields->segments % fields->threads != 0) {
        segment_not_multiple_err(lineNum);
        return false;
    }
    return true;
}

// Takes client's jobfile fields and line number as @param.
// Parsing client's jobfile fields 
// If all fields parsed and assigned successfully then 
// Return true else return false.
bool parse_fields(char* line, ClientFields* fields) {
    char** functionValues = split_by_char(line, ',', 2);
    char* function = functionValues[0];
    char* values = functionValues[1];
    if (null_or_empty(function) || null_or_empty(values)) {
        free(functionValues);
        return false;
    }
    double upper, lower;
    long segments, threads;
    char emptyC;

    if (sscanf(values, "%lf,%lf,%ld,%ld%c", 
            &lower, &upper, &segments, &threads, &emptyC) != 4) {
        free(functionValues);
        return false;
    }
    if (segments > INT_MAX || threads > INT_MAX) {
        free(functionValues);
        return false;
    }
    fields->function = strdup(function);
    fields->lower = lower;
    fields->upper = upper;
    fields->segments = (int) segments;
    fields->threads = (int) threads;
    free(functionValues);
    return true;
}

// Takes readClientField file pointer, server connection 
// and vFlag status as @param.
// Reads the given jobfile or stdin and checks if its not empty or is comment.
// Parses the fields on each line and if unsuccessful then prints syntax err.
// Verifies the parsed fields on each line and checks if they are valid.
// Sends the function to the server to validate.
// Server integrates the function and sends the result back which is printed.
// If integration is unsucessful then prints integration fail err. 
void parse_verify_integrate(FILE* readClientField, 
        ServerConnection* sc, bool vFlag) {
    char* line;
    int lineNum = 0;
    ClientFields integ;
    double integrationResult;
    while ((line = read_line(readClientField))) {
        ++lineNum;
        if (!strlen(line) || is_empty(line) || is_comment(line)) {
            continue;
        }
        if (!parse_fields(line, &integ)) {
            syntax_err(lineNum);
            continue;
        }
        if (!verify_fields(&integ, lineNum)) {
            continue;
        }
        if (!validate_function_to_server(integ.function, sc, lineNum)) {
            continue;
        }
        if (integrate_to_server(&integ, sc, vFlag, &integrationResult)) {
            printf("The integral of %s from %lf to %lf is %lf\n", 
                    integ.function, integ.lower, 
                    integ.upper, integrationResult);
            fflush(stdout);
        } else {
            integration_fail();
        }
    }
}

int main(int argc, char** argv) {
    if (argc < 2 || argc > 4) {
        client_usage_err();
    }
    bool vFlag = check_verbose(argc, argv);
    char* port = (vFlag) ? argv[2] : argv[1];

    FILE* readClientField = stdin;
    FILE* closeMe = NULL;
    int fIndex = 0;
    if (vFlag && argc == 4) {
        fIndex = 3;
    } else if (!vFlag && argc == 3) {
        fIndex = 2;
    }
    if (fIndex) {
        readClientField = fopen(argv[fIndex], "r");
        if (!readClientField) {
            file_err(argv[fIndex]);
        }
        closeMe = readClientField;
    }
    int server = connect_to_server(port);
    ServerConnection sc;
    sc.server = server;
    sc.writeRequest = fdopen(server, "w");
    sc.readResponse = fdopen(dup(server), "r");

    parse_verify_integrate(readClientField, &sc, vFlag);
    if (closeMe) {
        fclose(closeMe);
    }
    close(server);
    return 0;
}