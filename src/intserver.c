#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <math.h>
#include <pthread.h>
#include <tinyexpr.h>
#include <csse2310a3.h>
#include <csse2310a4.h>
#include <signal.h>
#include "errorHandling.h"

#define MAX_PORTNUM 65535
#define STATUS_OK 200
#define BAD_STATUS 400
#define BUFSIZE 1000
#define HTTP_BUFF_SIZE 10000
#define TEMP_BUFF_SIZE 500

// Structure to store HTTP request details.
typedef struct Request {
    char* method;
    char* address;
    HttpHeader** headers;
    char* body;
} Request;

// Structure to store client connection details.
typedef struct ClientConnection {
    int fd;
    FILE* readFile;
    FILE* writeFile;
} ClientConnection;

// Structure to store computational thread details.
typedef struct Computation {
    char* function;
    double a;
    double b;
    double w;
    double s;
    double wi;
    double result;
} Computation;

// CHECKING COMMAND LINE ARGUMENTS ---------------------------------------

// Takes a string as @param.
// Checks if the string is a number.
// Retruns true if it is else returns false.
bool is_num(char* str) {
    if (!str || !strlen(str)) {
        return false;
    }
    int i = 0;
    while (str[i] != '\0') {
        if (!isdigit(str[i])) {
            return false;
        }
        i++;
    }
    return true;
}

// Takes a string as @param.
// Converts the string to number.
// Checks if the number is a positive integer.
// Retruns true if it is else returns false.
bool check_positive(char* str) {
    if (is_num(str)) {
        int num = atoi(str);
        if (num <= 0) {
            return false;
        }
    } else {
        return false;
    }
    return true;
}

// Takes argc and argv as @param.
// Checks if the command line arguments are valid. 
// Checks if argc either is less than 2 or greater than 3.
// Checks if portnum is integer and between 0 and 65535.
// Checks if maxthreads is positive if present.
// If these conditions are valid then returns true else returns false. 
bool valid_args(int argc, char** argv) {
    if (argc < 2 || argc > 3) {
        return false;
    }
    if (is_num(argv[1])) {
        if ((atoi(argv[1]) < 0) || (atoi(argv[1]) > MAX_PORTNUM)) {
            return false;
        }
    } else {
        return false;
    }
    if (argc == 3 && !check_positive(argv[2])) {
        return false;
    }
    return true;
}

// SIGHUP HANDLING--------------------------------------------------------

// Program statistics mutex
pthread_mutex_t lockStats = PTHREAD_MUTEX_INITIALIZER;  

// (at this instant)
static int connectedClients = 0;
// since program start
static int checkedExpressions = 0;
// since program start
static int jobsCompleted = 0;
// since program start
static int badJobs = 0;
// (since program start, including any currently active)
static int totalThreads = 0;

// Tinyexpr mutex
pthread_mutex_t lockTinyExpr = PTHREAD_MUTEX_INITIALIZER;  

// Maxthreads mutex
pthread_mutex_t lockMaxThread = PTHREAD_MUTEX_INITIALIZER;  
// Global variable for maxThreads.
static int maxThreads = INT_MAX;
// Keeps a track of cuurent total threads alive.
static int currTotalThreads = 0;

// Blocks the individual client handling threads and does not launch a new 
// worker thread until currTotalThreads is less than the maximum.
void sleep_until_ready_to_launch(void) {
    if (maxThreads == INT_MAX) {
        // dont wait
        return;
    }
    while (true) {
        pthread_mutex_lock(&lockMaxThread);
        // check if new thread can be launched
        if (currTotalThreads < maxThreads) { 
            currTotalThreads++;
            pthread_mutex_unlock(&lockMaxThread);
            // return and launch thread
            return;
        }
        pthread_mutex_unlock(&lockMaxThread);
        sleep(1); //sleep little bit
    }
}

// Takes signal number as @param.
// Prints statistics  to stderr reflecting the program’s operation.
// Upon receiving SIGHUP signal. 
void print_sighup(int sigNum) {
    pthread_mutex_lock(&lockStats);
    fprintf(stderr, "Connected clients:%d\n", connectedClients);
    fprintf(stderr, "Expressions checked:%d\n", checkedExpressions);
    fprintf(stderr, "Completed jobs:%d\n", jobsCompleted);
    fprintf(stderr, "Bad jobs:%d\n", badJobs);
    fprintf(stderr, "Total threads:%d\n", totalThreads);
    pthread_mutex_unlock(&lockStats);
}

// Takes integer change as @param.
// Changes connected clients (thread safe method)
void change_connected_clients(int change) {
    pthread_mutex_lock(&lockStats);
    connectedClients += change;
    pthread_mutex_unlock(&lockStats);
}

// Increments checkedExpressions (thread safe method)
void another_expression_checked(void) {
    pthread_mutex_lock(&lockStats);
    checkedExpressions += 1;
    pthread_mutex_unlock(&lockStats);
}

// Increments jobsCompleted (thread safe method)
void another_job_success(void) {
    pthread_mutex_lock(&lockStats);
    jobsCompleted += 1;
    pthread_mutex_unlock(&lockStats);
}

// Increments badJobs (thread safe method)
void another_job_fail(void) {
    pthread_mutex_lock(&lockStats);
    badJobs += 1;
    pthread_mutex_unlock(&lockStats);
}

// Increments totalThreads (thread safe method)
void another_thread_started(void) {
    pthread_mutex_lock(&lockStats);
    totalThreads += 1;
    pthread_mutex_unlock(&lockStats);
}

// Takes HTTP buffer as @param.
// Parses HTTP request details and retruns the request.
Request* parse_request(char* buffer) {
    Request* request = malloc(sizeof(Request));
    memset(request, 0, sizeof(Request));
    int size = parse_HTTP_request(buffer, strlen(buffer), &request->method,
            &request->address, &request->headers, &request->body);
    if (size <= 0) {
        free(request);
        return NULL;
    }
    return request;
}

// Takes function as @param.
// Validates the function (f(x)) using tinyexpr library.
// If successful then returns true else returns false.
bool validate_x_function(char* function) {
    double x;           
    te_variable vars[] = {{"x", &x}};
    int errPos;

    te_expr* expr = te_compile(function, vars, 1, &errPos);

    if (expr) {
        te_free(expr);
        return true;

    } else {
        return false;
    }
    return true;
}

// Takes function, width, upper and lower bound as @param.
// Evaluates the expression and calculates the area under the curve.
// Integrates from a to b with width wi. 
// Returns the result of integration.
double integration_x(char* function, double wi, double a, double b) {
    double x;           
    te_variable vars[] = {{"x", &x}};
    int errPos;

    te_expr* expr = te_compile(function, vars, 1, &errPos);
    
    x = a;
    double fa, fb;
    fa = te_eval(expr);
    x = b;
    fb = te_eval(expr);
    // area under the curve
    double result = ((fa + fb) / 2.0) * wi;
    te_free(expr);
    return result;
}

// Takes computation as @param.
// Computational thread computes and returns computation.
void* computation_thread(void* arg) {
    another_thread_started();
    Computation* comp = (Computation*) arg;
    pthread_mutex_lock(&lockTinyExpr);
    comp->result = 0;
    double lower = comp->a;
    for (int s = 1; s <= comp->s; s++) {
        double ai = lower + (s - 1) * comp->w;
        double bi = lower + s * comp->w;
        comp->result += integration_x(comp->function, comp->w, ai, bi);
    }
    pthread_mutex_unlock(&lockTinyExpr);
    pthread_mutex_lock(&lockMaxThread);
    currTotalThreads--;
    pthread_mutex_unlock(&lockMaxThread);
    return comp;
}

// Takes function, lower and upper bounds, width, segments 
// and width of the region assigned to thread i.
// Creates new Computation request and returns it. 
Computation* new_computation_request(char* function, 
        double a, double b, double w, double s, double wi) {
    Computation* comp = malloc(sizeof(Computation));
    comp->result = 0;
    comp->a = a;
    comp->b = b;
    comp->w = w;
    comp->s = (b - a) / w;
    comp->wi = wi;
    comp->function = function;
    return comp;
}

// Takes function, lower and upper bounds, segments, threads 
// and computation as @params.
// Performs integraion from a to b with s segments and t threads, 
// and stores partialResults at Computations
double perform_integration(char* function,
        double a, double b, int s, int t, Computation** computations) {
    pthread_t threads[t];
    double wi = (b - a) / ((double)t);
    double w = (b - a) / ((double)s);

    for (int threadNum = 1; threadNum <= t; threadNum++) {
        
        double ai = a + (threadNum - 1) * wi;
        double bi = a + threadNum * wi;

        sleep_until_ready_to_launch();
        pthread_create(&threads[threadNum - 1],
                NULL,
                computation_thread,
                new_computation_request(function, ai, bi, w, s, wi));
    }
    double result = 0;
    
    for (int i = 0; i < t; i++) {
        pthread_join(threads[i], (void**)&computations[i]);
        result += computations[i]->result;
    }
    return result;
}

// Takes Content-Length as @param.
// Creates new HttpHeader array with single valid 
// header included for content-length.
// Returns headers.
HttpHeader** headers_array(int contentLen) {
    HttpHeader** headers = malloc(sizeof(HttpHeader *) * 10);
    memset(headers, 0, sizeof(HttpHeader *) * 10);
    headers[0] = malloc(sizeof(HttpHeader));
    headers[0]->name = strdup("Content-Length");
    headers[0]->value = malloc(10);
    sprintf(headers[0]->value, "%d", contentLen);
    return headers;
}

// Takes Client connection as @param.
// Sends http 400 response to client indicating a Bad Request.
// Response is sent using writeFile file pointer.
// Freeing the response and headers. 
void send_bad_request(ClientConnection* connection) {
    HttpHeader** headers = headers_array(0);
    char* body = "";
    char* response = 
            construct_HTTP_response(BAD_STATUS, "Bad Request", headers, body);
    fprintf(connection->writeFile, "%s", response);
    fflush(connection->writeFile);
    free(response);
    free_array_of_headers(headers);
}

// Takes function and Client connection as @param.
// Accepts validate function request from client.
// If validation was successful then 
// creates a response returns OK (200) else Bad Request (400).
// Freeing the response and headers. 
void validate_request(char* function, ClientConnection* connection) {
    another_expression_checked();
    int status;
    char* text;
    if (validate_x_function(function)) {
        status = STATUS_OK;
        text = "OK";
    } else {
        status = BAD_STATUS;
        text = "Bad Request";
    }
    HttpHeader** headers = headers_array(0);
    char* body = "";
    char* response = construct_HTTP_response(status, text, headers, body);
    fprintf(connection->writeFile, "%s", response);
    fflush(connection->writeFile);
    free(response);
    free_array_of_headers(headers);
}

// Takes readFile file pointer as @param.
// Reads as many lines as possible as HTTP buffer, 
// stops reading when encounters an empty new line.
// Checks if the content in the temp buffer is equal to the content- length.
// Returns the HTTP buffer that was read. 
char* read_http_buffer(FILE* readFile) {
    char* buffer = malloc(HTTP_BUFF_SIZE);
    memset(buffer, 0, HTTP_BUFF_SIZE);
    int limit = BUFSIZE;
    int used = 0;

    char tempBuffer[TEMP_BUFF_SIZE];
    bool thereIsContent = false;
    int contBytes = 0;
    while (true) {
        if (!fgets(tempBuffer, TEMP_BUFF_SIZE, readFile)) {
            // client disconnect
            free(buffer);
           return NULL;
        }
        strcat(buffer, tempBuffer);
        used += strlen(tempBuffer);
        if (used >= limit) {
            limit += BUFSIZE;
            buffer = realloc(buffer, limit);
        }
        if (!strncmp("Content-Length: ", tempBuffer, 
                strlen("Content-Length: "))) {
            if (strncmp("Content-Length: 0", tempBuffer, 
                    strlen("Content-Length: 0"))) {
                sscanf(&tempBuffer[strlen("Content-Length: ")], 
                        "%d", &contBytes);
                thereIsContent = true;
            }
        }
        if (!strcmp(tempBuffer, "\r\n") || !strcmp(tempBuffer, "\n\n") 
                || !strcmp(tempBuffer, "\n")) {
            break;
        }
    }
    if (thereIsContent) {
        while (contBytes > 1) {
            if (!fgets(tempBuffer, contBytes + 1, readFile)) {
                // client disconnects
                return NULL;
            }
            strcat(buffer, tempBuffer);
            contBytes -= strlen(tempBuffer);
        }
    }
    if (buffer[strlen(buffer) - 1] != '\n') {
        buffer[strlen(buffer) - 1] = '\n';
    }
    return buffer;
}

// Takes threads, computations, a, b, results and HTTP body as @param.
// Process partial results and prints it. 
void process_partial_result(int threads, Computation** computations, 
        double a, double b, double result, char* body) {
    for (int i = 0; i < threads; i++) {
        double pa = computations[i]->a;
        double pb = computations[i]->b;
        double partialResult = computations[i]->result;
        char partialLine[100];
        sprintf(partialLine, "thread %d:%lf->%lf:%lf\n", i + 1,
                pa, pb, partialResult);
        strcat(body, partialLine);
        free(computations[i]);
    }
    char finalLine[100];
    printf(finalLine, "%lf\n", result);
    strcat(body, finalLine);
}

// Takes functionValues, Client connection and i as @params.
// Checks if the request is unsuccessful and job fails.
// Print Bad Request and status 400
void check_bad_request(char** functionValues, 
        ClientConnection* connection, int i) {
    if (!functionValues[i] || !strlen(functionValues[i])) {
        another_job_fail();
        send_bad_request(connection);
        return;
    }   
}

// Takes HTTP requesta address, Client connection 
// and verbose flag status as @param.
// Handles a new integration request from a client
// Spawning threads to do the computation in parallel
// Creates a response returns OK (200) on success.
void integrate_request(char* address, 
        ClientConnection* connection, bool verbose) {
    char** functionValues = split_by_char(address, '/', 5);
    for (int i = 0; i < 5; i++) {
        check_bad_request(functionValues, connection, i);
    }
    if (functionValues[5] || !validate_x_function(functionValues[4])) {
        another_job_fail();
        send_bad_request(connection);
        return;
    }
    double lower, upper;
    int segments, threads;
    long seg, thr;
    char n;
    if (sscanf(functionValues[0], "%lf%c", &lower, &n) != 1 ||
            sscanf(functionValues[1], "%lf%c", &upper, &n) != 1 ||
            sscanf(functionValues[2], "%ld%c", &seg, &n) != 1 ||
            sscanf(functionValues[3], "%ld%c", &thr, &n) != 1 ||
            upper <= lower || seg <= 0 || thr <= 0 || (seg % thr != 0) ||
            seg > INT_MAX || thr > INT_MAX) {
        another_job_fail();
        send_bad_request(connection);
        return;
    }
    char* function = functionValues[4];
    segments = seg;
    threads = thr;
    double a = lower;
    double b = upper;
    Computation** computations = malloc(sizeof(Computation*) * threads);
    double result = perform_integration(function, a, 
            b, segments, threads, computations);
    char* body = malloc(1000);
    body[0] = '\0';
    if (!verbose) {
        sprintf(body, "%lf\n", result);

    } else {
        process_partial_result(threads, computations, a, b, result, body);
    }
    HttpHeader** headers = headers_array(strlen(body));    
    char* response = construct_HTTP_response(STATUS_OK, "OK", headers, body);
    fprintf(connection->writeFile, "%s", response);
    fflush(connection->writeFile);
    free(response);
    free_array_of_headers(headers);
    free(body);
    another_job_success();
}

// Takes Client connection, HTTP buffer and request as @param.
// Attempts to achieve a client thread connection. 
// Creating a new request and parsing it.
// Checking if its validate or integrate address by comparing GET method.
// If validate then validates the function 
// and returns response with status 200 or 400.
// If integrate then integrates the fucntion returns 200 
// with result or 400 if integration fails.
// if method not GET or address not valid then its 
// bad request end connection and break loop.
// Close the IO ends and terminate the thread. 
void* client_thread(void* arg) {
    change_connected_clients(1);
    ClientConnection* connection = (ClientConnection*)arg;
    char* buffer;
    while (true) {
        buffer = read_http_buffer(connection->readFile);
        if (!buffer) {
            break;
        }
        // http request
        Request* request = parse_request(buffer);
        if (!request) {
            break;
        }
        // check if its validate or integrate address
        if (strcmp("GET", request->method)) {
            // request not a GET request
            send_bad_request(connection);
            continue;
        }
        if (!strncmp("/validate/", request->address, 10)) {
            // validation request
            char* func = request->address + 10;
            validate_request(func, connection);

        } else if (!strncmp("/integrate/", request->address, 11)) {
        // integration request /integrate/lwoer/upper/segments/threads/function
            integrate_request(request->address + 11, connection, false);
        } else {
            // unknown request adddress
            send_bad_request(connection);
            continue;
        }
    }

    close(connection->fd);
    fclose(connection->writeFile);
    fclose(connection->readFile);
    free(connection);
    change_connected_clients(-1);
    pthread_exit(NULL);
}

// NETWORKING AND CLIENT THREAD CREATION----------------------------------

// Used reference code provided in the lectures.
// Listens on given port. Returns listening socket (or exits on failure)
int open_listen(const char* port) {
    struct addrinfo* ai = 0;
    struct addrinfo hints;
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_INET;   // IPv4
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;    // listen on all IP addresses
    int err;
    if ((err = getaddrinfo(NULL, port, &hints, &ai))) {
        freeaddrinfo(ai);
        socket_err();
    }
    
    // Create a socket and bind it to a port
    int listenFd = socket(AF_INET, SOCK_STREAM, 0); // 0=default protocol (TCP)

    // Allow address (port number) to be reused immediately
    int optVal = 1;
    if (setsockopt(listenFd, SOL_SOCKET, SO_REUSEADDR, 
            &optVal, sizeof(int)) < 0) {
        socket_err();
    }

    if (bind(listenFd, (struct sockaddr*) ai->ai_addr, 
            sizeof(struct sockaddr))) {
        socket_err();
    }
    
    struct sockaddr_in ad;
    memset(&ad, 0, sizeof(struct sockaddr_in));
    socklen_t socketLen = sizeof(struct sockaddr_in);
    if (getsockname(listenFd, (struct sockaddr*) &ad, &socketLen)) {
        socket_err();
    }
    fprintf(stderr, "%u\n", ntohs(ad.sin_port));
    fflush(stderr);
    if (listen(listenFd, SOMAXCONN) < 0) {
        socket_err();
    }
    return listenFd;
}

// Used reference code provided in the lectures.
// Takes the server's file descriptor as @param.
// Accepts connections to the port and starts 
// a client thread at each new sucessful connection.
void process_connections(int fdServer) {
    int fd;
    // Repeatedly accept connections and process data 
    while (1) {
        fd = accept(fdServer, 0, 0);
        if (fd < 0) {
            continue;
        }
        // real connection
        ClientConnection* connection = malloc(sizeof(ClientConnection));
        connection->fd = fd;
        connection->readFile = fdopen(fd, "r");
        connection->writeFile = fdopen(dup(fd), "w");
        pthread_t threadId;
        pthread_create(&threadId, NULL, client_thread, connection);
        pthread_detach(threadId);
        
    }
}

int main(int argc, char** argv) {
    if (!valid_args(argc, argv)) {
        server_usg_err();
    }
    const char* portNum = argv[1];
    int listenFd = open_listen(portNum);
    signal(SIGHUP, print_sighup);
    process_connections(listenFd);
    return 0; 
}